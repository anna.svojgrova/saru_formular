import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class WebFormGoogle extends PageObject {

    private final String TITLE_XPATH = "//div[contains(text(),'Formulář')]";

    public WebFormGoogle(WebDriver driver) {
        super(driver);
    }
    public void findTitle() {
        driver.findElement(By.xpath(TITLE_XPATH));
    }

}
