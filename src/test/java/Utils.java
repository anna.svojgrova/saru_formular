// Configuration class for Form Automation project

public class Utils {
    final static String BASE_URL1 = "https://docs.google.com/forms/d/e/1FAIpQLSdPyLEWnYsTWkvfGT26aGwZNYMhWXo4rFx4-8ONfjML_OTTag/viewform";
    final static String BASE_URL2 = "https://formy-project.herokuapp.com/form";
    final static String CHROME_DRIVER_LOCATION = "chromedriver";
}