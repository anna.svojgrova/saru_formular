import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.Assert;
import org.testng.annotations.AfterSuite;
import org.testng.annotations.BeforeSuite;
import org.testng.annotations.Test;

public class TestPlan {
    private static final WebDriver driver = new ChromeDriver();

    @BeforeSuite
    public static void main(String[] args) {
        // ChromeDriver location set up in Utils class
        System.setProperty("webdriver.chrome.driver", Utils.CHROME_DRIVER_LOCATION);
    }

    @Test(testName = "Open the Google Form")
    public static void loadUrl() {
        driver.get(Utils.BASE_URL1);
        WebFormGoogle webFormGoogle = new WebFormGoogle(driver);
        webFormGoogle.findTitle();
    }

    @Test(testName = "Submit a Tutorial Web Form")
    public static void submitForm() {
        driver.get(Utils.BASE_URL2);
        WebFormTutorial webFormTutorial = new WebFormTutorial(driver);
        webFormTutorial.enterFirstName();
        webFormTutorial.enterLastName();
        webFormTutorial.pressSubmitButton();
    }

    @AfterSuite
    public static void cleanUp() {
        driver.manage().deleteAllCookies();
        driver.close();
    }
}